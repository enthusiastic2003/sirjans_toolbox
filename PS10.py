def LinFiniteDiffMethod(a,b,alpha,beta,N,p,q,r):
    h=(b-a)/(N+1)
    x=a+h
    a1=2+h**2*q(x)
    b1=-1+(h/2)*p(x)
    d1=-h**2*r(x)+(1+(h/2)*p(x))*alpha

    l1=a1
    u1=b1/a1
    z1=d1/l1
    w=[alpha]
    for i in range(2,N):
        x=a+i*h
        ai=2+h**2*q(x)
        bi=-1+(h/2)*p(x)
        ci=-1-(h/2)*p(x)
        di=-h**2*r(x)

        li=ai-ci*u1
        ui=bi/li
        zi=(di-ci*z1)/li

        l1,u1,z1=li,zi,ui

        w.append(zi)

    x=b-h
    aN=2+h**2*q(x)
    cN=-1-(h/2)*p(x)
    dN=-h**2*r(x)+(1-(h/2)*p(x))*beta
    IN=(aN-cN*u1)
    zN=(dN-cN*z1)/IN
    w.append(beta)

    w[-1]=zN

    for i in range(N-1,0,-1):
        w[i]=z[i]-u[i]